﻿using UnityEngine;

public class Turret : MonoBehaviour
{
    [SerializeField]
    private TurretInfo turretInfo;
    [SerializeField]
    private LayerMask EnemyLayerMask;
    [SerializeField]
    private Transform topModel;
    [SerializeField]
    private ParticleSystem[] muzzles;
    [SerializeField]
    private ParticleSystem explotion;
    [SerializeField]
    private AudioSource sound;
    private float lastShoot;
    private Enemy target;
    private Collider[] colliders;
    private void Update()
    {
        colliders = Physics.OverlapSphere(transform.position, turretInfo.radius, EnemyLayerMask);
        if (colliders.Length > 0)
        {
            if (Time.time - lastShoot >= turretInfo.shootSpeed)
            {
                target = colliders[0].transform.GetComponent<Enemy>();
                topModel.transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
                lastShoot = Time.time;
                Shoot();
            }
        }
    }

    private void Shoot()
    {
        sound.Play();
        for (int i = 0; i < muzzles.Length; i++)
        {
            muzzles[i].Play();
        }
        explotion.transform.position = new Vector3(target.transform.position.x, target.transform.position.y + 1f, target.transform.position.z);
        explotion.Play();
        target.TakeDamge(turretInfo.damage, turretInfo.addtribute, turretInfo.effectDuration);
    }
}

﻿using UnityEngine;

[CreateAssetMenu(menuName = "Giang/Turret Info")]
public class TurretInfo : ScriptableObject
{
    public float hp;
    public float damage;
    public Addtribute addtribute;
    public float effectDuration;
    public float radius;
    public float shootSpeed;
}

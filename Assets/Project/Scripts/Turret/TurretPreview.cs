﻿using UnityEngine;

public class TurretPreview : MonoBehaviour
{
    [SerializeField]
    private Renderer[] Models;
    public BoxCollider boxCollider;

    public void ChangeModelColors(Color color)
    {
        for (int i = 0; i < Models.Length; i++)
        {
            Models[i].material.color = color;
        }
    }
}

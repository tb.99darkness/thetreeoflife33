﻿using UnityEngine;
using UnityEngine.Events;

public class AnimatorEventNode : MonoBehaviour
{
    public UnityEvent HitRaise;

    public void Hit()
    {
        HitRaise?.Invoke();
    }
}

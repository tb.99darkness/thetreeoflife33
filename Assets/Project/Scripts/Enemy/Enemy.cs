﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    protected NavMeshAgent agent;
    [SerializeField]
    protected Animator animator;
    [SerializeField]
    protected float hp;
    [SerializeField]
    protected new CapsuleCollider collider;
    [SerializeField]
    protected float damage;
    [HideInInspector]
    public TheTree target;
    [SerializeField]
    protected float attackSpeed;
    [SerializeField]
    protected int coin;
    public Vector3 hitPos;
    private WaitForSeconds wait = new WaitForSeconds(0.5f);
    public WareManger wareManger;


    // Update is called once per frame
    [System.Obsolete]
    void Update()
    {
        float distance = target.navMeshObstacle.radius + agent.radius;
        float currentDistance = Vector3.Distance(transform.position, target.transform.position);
        if (currentDistance <= distance)
        {
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
            SetDestination(Vector3.zero);
            animator.SetTrigger("Hit");
        }
        else
        {
            SetDestination(target.transform.position);
        }
    }


    private void SetDestination(Vector3 targetPosition)
    {
        if (targetPosition != Vector3.zero)
        {
            agent.SetDestination(targetPosition);
            animator.SetTrigger("Walk");
        }
        else
        {
            agent.Stop();

        }
    }
    public virtual void TakeDamge(float dmg, Addtribute addtribute, float duration)
    {
        hp -= dmg;
        animator.SetTrigger("GetHit");
        if (hp <= 0)
        {
            Dead();
        }
    }

    public virtual void Dead()
    {
        StartCoroutine(Destroy());
    }

    public virtual void Hit()
    {
        target.TakeHit(damage);
    }

    IEnumerator Destroy()
    {
        agent.Stop();
        animator.SetTrigger("Die");
        collider.enabled = false;
        yield return wait;
        animator.StopPlayback();
        wareManger.UpdateCoin(coin);
        Destroy(gameObject);
    }

    public void Init(float hardLevel, TheTree target, WareManger wareManger)
    {
        this.hp *= hardLevel;
        this.target = target;
        transform.localScale *= hardLevel;
        this.damage *= hardLevel;
        this.coin *= (int)hardLevel;
        this.wareManger = wareManger;
        agent.radius = 0.1f;
    }
}

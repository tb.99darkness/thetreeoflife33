﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class TheTree : MonoBehaviour
{
    [SerializeField]
    private float hp;
    [SerializeField]
    private float rotateSpeed;
    [SerializeField]
    public NavMeshObstacle navMeshObstacle;
    [SerializeField]
    private FloatEvent theTreeHp;
    [SerializeField]
    private UnityEvent endGame;
    private void Start()
    {
        TakeHit(0);
    }
    private void Update()
    {
        transform.Rotate(new Vector3(0, rotateSpeed, 0));
    }

    public void TakeHit(float damage)
    {
        hp -= damage;
        theTreeHp?.Invoke(hp);
        if (hp <= 0)
        {
            endGame?.Invoke();
        }
    }
}

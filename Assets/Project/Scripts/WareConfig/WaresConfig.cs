﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Giang/WaresConfig")]
public class WaresConfig : ScriptableObject
{
    public WareConfig[] wareConfigs;
}

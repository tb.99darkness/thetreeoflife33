﻿using Sirenix.OdinInspector;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float speed, sen, baseGravity;
    [SerializeField]
    private CharacterController characterController;
    [SerializeField]
    private Camera camera;
    [SerializeField]
    private LayerMask enemyMask;
    [SerializeField]
    private LayerMask detectMask;
    [SerializeField]
    private LayerMask wallMask;
    [SerializeField]
    private LayerMask floorMask;
    [SerializeField]
    private Weapon currentWeapon;
    [SerializeField]
    private EnemyEvent enemyEvent;
    [SerializeField]
    private Vector3 craftPosition;
    [SerializeField]
    private bool isCrafting;
    [SerializeField]
    private bool canCraft;
    [SerializeField]
    private TurretPreview previewTurret;
    [SerializeField]
    private float craftDistance;
    [SerializeField]
    private Vector3Event craftPostionEvent;
    [SerializeField]
    private int coin;
    [SerializeField]
    private IntEvent UpdateCoin;

    private Vector3 previewTurretPosition;
    private float xRotation, yRotation;
    private Vector3 moveDirection = Vector3.zero;

    private void Awake()
    {
        previewTurret.gameObject.SetActive(false);
        previewTurret.transform.position = previewTurretPosition;
        previewTurretPosition.z = craftDistance;
        GetCoin(0);
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {

        if (Mathf.Abs(Input.GetAxis("Horizontal")) >= 0.1f || Mathf.Abs(Input.GetAxis("Vertical")) >= 0.1f)
        {
            Move(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")));
        }
        if (Mathf.Abs(Input.GetAxis("Mouse X")) >= 0.1f || Mathf.Abs(Input.GetAxis("Mouse Y")) >= 0.1f)
        {
            Rotate(new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")));
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Shoot();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (!isCrafting)
            {
                StartCrafting();
            }
            else
            {
                Crafted();
            }

        }
        if (isCrafting)
        {

            CraftDetect();
        }
    }
    [Button]
    public void StartCrafting()
    {
        previewTurret.gameObject.SetActive(true);
        isCrafting = true;
    }
    [Button]
    public void Crafted()
    {
        previewTurret.gameObject.SetActive(false);
        if (canCraft && coin >= 100)
        {
            GetCoin(-100);
            craftPostionEvent?.Invoke(craftPosition);
        }
        isCrafting = false;
    }
    public void CraftDetect()
    {
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.TransformDirection(Vector3.forward), out hit, craftDistance, detectMask))
        {
            craftPosition = new Vector3(hit.point.x, hit.point.y + previewTurret.boxCollider.size.y / 3, hit.point.z);
            previewTurret.transform.position = craftPosition;
            if ((floorMask & 1 << hit.transform.gameObject.layer) == 1 << hit.transform.gameObject.layer)
            {
                previewTurret.ChangeModelColors(Color.green);
                canCraft = true;
            }
            else if ((wallMask & 1 << hit.transform.gameObject.layer) == 1 << hit.transform.gameObject.layer)
            {
                craftPosition = Vector3.zero;
                previewTurret.ChangeModelColors(Color.red);
                canCraft = false;
            }
        }
        else
        {
            craftPosition = Vector3.zero;
            previewTurret.ChangeModelColors(Color.red);
            canCraft = false;
        }
    }

    public void Craft(Turret turret)
    {
        if (craftPosition != Vector3.zero)
        {
            Instantiate(turret, craftPosition, Quaternion.identity);
        }
    }

    [Button]
    public void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.TransformDirection(Vector3.forward), out hit, 1000, enemyMask))
        {
            Enemy enemy = hit.transform.GetComponent<Enemy>();
            enemy.hitPos = hit.point;
            enemyEvent?.Invoke(enemy);
        }
        else
        {
            enemyEvent?.Invoke(null);
        }
    }
    public void Move(Vector2 move)
    {
        moveDirection = new Vector3(move.x, 0, move.y);
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= speed * Time.deltaTime;
        moveDirection.y -= baseGravity * Time.deltaTime;
        characterController.Move(moveDirection);
    }


    public void Rotate(Vector2 rotate)
    {
        float mouseX = rotate.x * sen * Time.deltaTime;
        float mouseY = rotate.y * sen * Time.deltaTime;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        camera.transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0f);
        transform.Rotate(Vector3.up * mouseX);
    }

    public void GetCoin(int coin)
    {
        this.coin += coin;
        UpdateCoin?.Invoke(this.coin);
    }
}

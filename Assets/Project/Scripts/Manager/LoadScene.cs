﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField]
    private FloatEvent progress;

    private WaitForEndOfFrame wait;
    private void Awake()
    {
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }
    public void LoadSceneGamePlay()
    {
        wait = new WaitForEndOfFrame();
        StartCoroutine(OnLoadScene());
    }
    private IEnumerator OnLoadScene()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("GamePlay", LoadSceneMode.Single);
        while (!operation.isDone)
        {
            yield return wait;
        }
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}

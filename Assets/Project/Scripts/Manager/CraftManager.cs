﻿using UnityEngine;

public class CraftManager : MonoBehaviour
{
    [SerializeField]
    private Turret selectedTurret;
    public void CraftTurret(Vector3 craftPosition)
    {
        Instantiate(selectedTurret, craftPosition, Quaternion.identity);
    }
    public void SetSelectedTurret(Turret turret)
    {
        selectedTurret = turret;
    }
}

﻿using TMPro;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    private FixedTouchField rotatePanel;
    [SerializeField]
    private FixedJoystick moveJoy;
    [SerializeField]
    private Vector2Event moveVector;
    [SerializeField]
    private Vector2Event rotateVector;
    [SerializeField]
    private TextMeshProUGUI coin;
    [SerializeField]
    private TextMeshProUGUI armo;

    private void Awake()
    {
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }
    private void Update()
    {
        if (rotatePanel.Pressed)
        {
            rotateVector?.Invoke(rotatePanel.TouchDist);
        }
        if (Mathf.Abs(moveJoy.Horizontal) >= 0.1f || Mathf.Abs(moveJoy.Vertical) >= 0.1f)
        {
            moveVector?.Invoke(moveJoy.Direction);
        }
    }

    public void UpdateCoin(int coin)
    {
        this.coin.text = "Coin :" + coin;
    }

    public void UpdateArmo(int armo)
    {
        this.armo.text = armo + "/30";
    }
}

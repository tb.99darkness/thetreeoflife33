﻿using System.Collections;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    protected WeaponInfo weaponInfo;
    [SerializeField]
    protected IntEvent updateArmo;
    [SerializeField]
    protected AudioSource sound;
    protected int numberOfArmo;
    protected float lastShoot;
    private void Start()
    {
        Init();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(Reloading());
        }
    }
    private void Init()
    {
        numberOfArmo = weaponInfo.maxArmo;
        updateArmo?.Invoke(numberOfArmo);
    }
    public virtual void Shoot(Enemy enemy)
    {
        if (numberOfArmo > 0)
        {

            if (Time.time - lastShoot >= weaponInfo.shootSpeed)
            {
                enemy.TakeDamge(weaponInfo.damage, weaponInfo.addtribute, weaponInfo.effectDuration);
                numberOfArmo--;
                updateArmo?.Invoke(numberOfArmo);
            }
            else
            {
                Debug.Log("Delay");
                return;
            }
        }
        else
        {
            Debug.Log("Het dan !");
            return;
        }
    }
    public void Reload()
    {
        StartCoroutine(Reloading());
    }

    private IEnumerator Reloading()
    {
        yield return new WaitForSeconds(2f);
        numberOfArmo = weaponInfo.maxArmo;
        updateArmo?.Invoke(numberOfArmo);
    }
}

﻿using UnityEngine;

public class Rife : Weapon
{
    [SerializeField]
    private ParticleSystem explosion;
    [SerializeField]
    private ParticleSystem muzzle;
    public override void Shoot(Enemy enemy)
    {
        if (numberOfArmo > 0)
        {
            if (Time.time - lastShoot >= weaponInfo.shootSpeed)
            {
                muzzle.Play();
                sound.Play();
                if (enemy != null)
                {
                    enemy.TakeDamge(weaponInfo.damage, weaponInfo.addtribute, weaponInfo.effectDuration);
                    Laser(enemy);
                }
                numberOfArmo--;
                updateArmo?.Invoke(numberOfArmo);
            }
            else
            {
                Debug.Log("Delay");
                return;
            }
        }
        else
        {
            Reload();
            return;
        }
    }

    private void Laser(Enemy enemy)
    {
        explosion.transform.position = enemy.hitPos;
        explosion.Play();
    }

}
